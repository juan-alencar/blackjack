#include <stdio.h>



void linha(void)
{
	printf("\n\n");
	printf("########################################################################################################################\n");
	printf("\n");
}

void linha2(void)
{
	printf("\n\n");
	printf("nununununununununununununununununununununununununununununununnununununununununununununununununununununununununununununun\n");
	printf("\n");
}

void cabecalho(void)
{
	printf("########################################################################################################################\n");
	printf("\t\t     ____    _                    _____   _  __       _               _____   _  __		\n");
	printf("\t\t    |  _ \\  | |          /\\      / ____| | |/ /      | |     /\\      / ____| | |/ /		\n");
	printf("\t\t    | |_) | | |         /  \\    | |      | ' /       | |    /  \\    | |      | ' / 		\n");
	printf("\t\t    |  _ <  | |        / /\\ \\   | |      |  <    _   | |   / /\\ \\   | |      |  <  	\n");
	printf("\t\t    | |_) | | |____   / ____ \\  | |____  | . \\  | |__| |  / ____ \\  | |____  | . \\ 	\n");
	printf("\t\t    |____/  |______| /_/    \\_\\  \\_____| |_|\\_\\  \\____/  /_/    \\_\\  \\_____| |_|\\_\\ \n");
	printf("\n\n\n");
}

void menu_player (void)
{
	printf ("\t\t\t\t\t\t(1) - 1 PLAYER\n");
	printf ("\t\t\t\t\t\t(2) - 2 PLAYERS\n");
	printf ("\t\t\t\t\t\t(3) - HIST�RICO\n");
	printf ("\t\t\t\t\t\t(4) - REGRAS\n");
	printf ("\t\t\t\t\t\t(5) - CR�DITOS\n");
    printf ("\t\t\t\t\t\t(6) - SAIR\n");
}

void historico(void)
{
    printf("########################################################################################################################\n");
    printf("\t\t\t  _    _   _____    _____   _______    ____    _____    _____    _____    ____  		\n");
    printf("\t\t\t | |  | | |_   _|  / ____| |__   __|  / __ \\  |  __ \\  |_   _|  / ____|  / __ \\ 		\n");
    printf("\t\t\t | |__| |   | |   | (___      | |    | |  | | | |__) |   | |   | |      | |  | |		\n");
    printf("\t\t\t |  __  |   | |    \\___ \\     | |    | |  | | |  _  /    | |   | |      | |  | |		\n");
    printf("\t\t\t | |  | |  _| |_   ____) |    | |    | |__| | | | \\ \\   _| |_  | |____  | |__| |		\n");
    printf("\t\t\t |_|  |_| |_____| |_____/     |_|     \\____/  |_|  \\_\\ |_____|  \\_____|  \\____/ 	\n\n");
    printf("########################################################################################################################\n");
    printf("\n");
    printf ("\t\t\t\t\t\tHIST�RICO DE PARTIDAS \n");
    printf("\n\n");
    
	FILE *arquivo;
	
        arquivo = fopen("hist.txt","r+");
        
        char hist[1000];
        
        while(fgets(hist,1000,arquivo) != NULL)
		{
            printf("%s",hist);
        }
        
    fflush(arquivo);
    
    printf("\n\n\n\n\n\n\n");
    printf("########################################################################################################################\n");
}

void regras(void)
{
	printf("########################################################################################################################\n");
	printf("\t\t\t\t   _____    ______    _____   _____                _____  		 	\n");
	printf("\t\t\t\t  |  __ \\  |  ____|  / ____| |  __ \\      /\\      / ____| 		\n");
	printf("\t\t\t\t  | |__) | | |__    | |  __  | |__) |    /  \\    | (___    		\n");
	printf("\t\t\t\t  |  _  /  |  __|   | | |_ | |  _  /    / /\\ \\    \\___ \\   		\n");
	printf("\t\t\t\t  | | \\ \\  | |____  | |__| | | | \\ \\   / ____ \\   ____) | 		\n");
	printf("\t\t\t\t  |_|  \\_\\ |______|  \\_____| |_|  \\_\\ /_/    \\_\\ |_____/  	\n\n");
	printf("########################################################################################################################\n");
	printf("\n");
    printf ("\t\t\t\tCada jogador recebe uma carta aleat�ria e virada para cima por vez,\n");
	printf ("\t\t\t\tPodendo ele decidir se quer outra carta ou n�o.\n\n");
	printf ("\t\t\t\tCada carta possui um valor,\n");
	printf ("\t\t\t\tE o objetivo � conseguir a maior pontua��o dentre os outros jogadores,\n");
    printf ("\t\t\t\tMas com cuidado para n�o ultrapassar a quantidade de 21 pontos,\n");
    printf ("\t\t\t\tPois caso ocorra o jogador perde automaticamente.\n\n");
	printf ("\t\t\t\tValor das cartas :\n\n");
	printf ("\t\t\t\t* O �s vale 1 ponto.\n");
	printf ("\t\t\t\t* O Valete, a Dama e o Rei valem 6, 8 e 10 pontos, respectivamente.\n");
    printf ("\t\t\t\t* As demais cartas, seu pr�prio valor de acordo com seu n�mero.\n");
    printf("\n\n");
	printf("########################################################################################################################\n");
	printf("\n");
}

void creditos(void)
{
	printf("########################################################################################################################\n");
	printf("\t\t\t   _____   _____    ______   _____    _____   _______    ____     _____ 	\n");
	printf("\t\t\t  / ____| |  __ \\  |  ____| |  __ \\  |_   _| |__   __|  / __ \\   / ____|		\n");
	printf("\t\t\t | |      | |__) | | |__    | |  | |   | |      | |    | |  | | | (___  	\n");
	printf("\t\t\t | |      |  _  /  |  __|   | |  | |   | |      | |    | |  | |  \\___ \\ 	\n");
	printf("\t\t\t | |____  | | \\ \\  | |____  | |__| |  _| |_     | |    | |__| |  ____) |	\n");
	printf("\t\t\t  \\_____| |_|  \\_\\ |______| |_____/  |_____|    |_|     \\____/  |_____/ 	\n\n");

	printf("########################################################################################################################\n");
	printf("\n\n\n");
	printf ("\t\t\t\tHayla Maria Lira Perazzo CCp1, RGM: 24795313\n\n");
	printf ("\t\t\t\tJuan Diego Medeiros Alencar CCp1 , RGM: 24887986\n\n");
	printf ("\t\t\t\tRebeca Maria Fialho Camilo CCp1 , RGM: 24874892\n\n");
	printf ("\t\t\t\tDanyel Bispo de Oliveira CCp1 , RGM: 24744336\n\n");
    printf("\n\n\n\n");
	printf("########################################################################################################################\n");

}

void fimdejogo(void)
{
    printf("########################################################################################################################\n\n\n\n\n\n\n\n\n");
    printf("\t\t\t  ______   _   _   _____       _____              __  __   ______ \n");
    printf("\t\t\t |  ____| | \\ | | |  __ \\     / ____|     /\\     |  \\/  | |  ____|\n");
    printf("\t\t\t | |__    |  \\| | | |  | |   | |  __     /  \\    | \\  / | | |__   \n");
    printf("\t\t\t |  __|   | . ` | | |  | |   | | |_ |   / /\\ \\   | |\\/| | |  __|  \n");
    printf("\t\t\t | |____  | |\\  | | |__| |   | |__| |  / ____ \\  | |  | | | |____  \n");
    printf("\t\t\t |______| |_| \\_| |_____/     \\_____| /_/    \\_\\ |_|  |_| |______|\n");
    printf("\n\n\n\n\n\n\n\n\n\n\n########################################################################################################################\n");
}



void pc_wins(void)
{
	printf("\n\n");
	printf("\t\t\t\t\t\t     __________			\n");
	printf("\t\t\t\t\t\t    / ======== \\		\n");
	printf("\t\t\t\t\t\t   / ___________\\		\n");
	printf("\t\t\t\t\t\t  | ____________ |		\n");
	printf("\t\t\t\t\t\t  | |   -YOU   | |		\n");
	printf("\t\t\t\t\t\t  | |   LOSE   | |		\n");
	printf("\t\t\t\t\t\t  | |__________| |		\n");
	printf("\t\t\t\t\t\t  \\=_____________/     \n");
	printf("\t\t\t\t\t\t  / ************ \\     \n");
	printf("\t\t\t\t\t\t / :::::::::::::: \\    \n");
	printf("\t\t\t\t\t\t(__________________)	\n");
	printf("\n");
}

void player_wins(void)
{

	printf("\t\t\t\t\t\t  _______________	    \n");
    printf("\t\t\t\t\t\t |@@@@|     |####|		\n");
    printf("\t\t\t\t\t\t |@@@@|     |####|		\n");
    printf("\t\t\t\t\t\t |@@@@|     |####|		\n");
    printf("\t\t\t\t\t\t \\@@@@|     |####/		\n");
    printf("\t\t\t\t\t\t  \\@@@|     |###/		\n");
    printf("\t\t\t\t\t\t   `@@|_____|##'		\n");
    printf("\t\t\t\t\t\t        (O)				\n");
    printf("\t\t\t\t\t\t     .-'''''-.			\n");
    printf("\t\t\t\t\t\t   .'  * * *  `.		\n");
    printf("\t\t\t\t\t\t  :  *       *  :		\n");
    printf("\t\t\t\t\t\t : ~   Y O U   ~ :		\n");
    printf("\t\t\t\t\t\t : ~  W I N S  ~ :		\n");
    printf("\t\t\t\t\t\t  :  *       *  :		\n");
    printf("\t\t\t\t\t\t   `.  * * *  .'		\n");
    printf("\t\t\t\t\t\t     `-.....-'			\n");
}

void empate(void)
{

    printf("\t\t\t\t\t     __..,,... .,,,,,.				\n");
    printf("\t\t\t\t\t ''''        ,'        ` .			\n");
    printf("\t\t\t\t\t           ,'  ,.  ..      `  .		\n");
    printf("\t\t\t\t\t           `.,'      ..           `	\n");
    printf("\t\t\t\t\t __..,.             .  ..     .		\n");
    printf("\t\t\t\t\t        ` .       .  `.  .` `			\n");
    printf("\t\t\t\t\t            ,  `.  `.  `._|,..		\n");
    printf("\t\t\t\t\t              .  `.  `..'				\n");
    printf("\t\t\t\t\t               ` -'`''				\n");

}
